import React from "react";
import { Routes, Route } from "react-router-dom";
import Login from "./page/Login";
import Register from "./page/Register";
import Page404 from "./page/Page404";
import Home from "./page/home/Home";      
import List from "./page/list/List";
import New from "./page/new/New";
import Single from "./page/single/Single";
import { userInputs } from "./formSource";


function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="home" element={<Home />} />
        <Route path="/" element={<Login />} />
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />}  />
        <Route path="*" element={<Page404 />} />
        <Route path="users">
          <Route index element={<List/>} />
          <Route path=":userId" element={<Single/>} />
          <Route
                path="new"
                element={<New inputs={userInputs} title="Add New User" />}
              />
        </Route>
        {/* <Route path="chart" element={<Chart />} /> */}
      </Routes>
    </div>
  );
}
export default App;
