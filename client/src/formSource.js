export const userInputs = [
    {
      id: 1,
      label: "Username",
      type: "text",
      placeholder: "Username",
    },
    {
      id: 2,
      label: "Name and surname",
      type: "text",
      placeholder: "Name and surname",
    },
    {
      id: 3,
      label: "Email",
      type: "mail",
      placeholder: "Emai",
    },
    {
      id: 4,
      label: "Phone",
      type: "text",
      placeholder: "Phone Number",
    },
    {
      id: 5,
      label: "Password",
      type: "password",
      placeholder: "password",
    },
    {
      id: 6,
      label: "Address",
      type: "text",
      placeholder: "Address",
    },
    {
      id: 7,
      label: "Country",
      type: "text",
      placeholder: "Country",
    },
  ];
