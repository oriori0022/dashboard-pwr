import React from 'react';
import { Link } from "react-router-dom";
// import { GoogleLogin } from "react-google-login";
// import { gapi } from "gapi-script";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { Component } from "react";
// import { useEffect } from "react";

// eslint-disable-next-line no-undef
export default class Login extends Component{
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit (e) {
    e.preventDefault(); //ป้องกันการเปลี่ยนหน้า
    const { email, password } = this.state;
    console.log(email, password);
    fetch("http://localhost:1337/users/login", {
      method: "POST",
      crossDomain: true,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "Access-Control-Allow-Origin": "http://localhost:1337",
      },

    body: JSON.stringify({
      email,
      password,
      expiresIn: 60000,
    }),
  })
      .then((response) => response.json())
      .then((data) => {
        console.log(data, "userLoginSuccess");
      });
  }

  render () {
    return (
    <div>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 16,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5" center>
            Log in
          </Typography>
          <Box
            component="form"
            Validate
            // onSubmit={handleSubmit}
            sx={{ mt: 1 }}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Enter email"
              name="email"
              // autoComplete="email"
              // autoFocus
              // value={inputs.email || ""}
              onChange={(e) => this.setState({ email: e.target.value })}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              // autoComplete="current-password"
              // value={inputs.password || ""}
              onChange={(e) =>
                this.setState({ password: e.target.value })}
            />
            <Link to="/Home">
              <input
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 1 }}
              >
              </input>
              </Link>

            <Link to="/Register">
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 2, mb: 2 }}
              >
              </Button>
            </Link>
            <Grid container></Grid>
          </Box>
        </Box>
      </Container>
    </div>
  );
}
  }

// export default Login;
