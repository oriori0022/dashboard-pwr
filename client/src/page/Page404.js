import React from "react";
import { Link } from "react-router-dom";
//import { Button } from "react";
function Page404() {
  return (
    <div>
      <h1>404 Page not found</h1>
      <Link to="/Home">
        <button> Go to Home </button>
      </Link>
    </div>
  );
}

export default Page404; 