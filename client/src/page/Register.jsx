import React from "react";
// import { Link } from "react-router-dom";
// import Button from "@mui/material/Button";
// import {input} from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { Component } from "react";

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fname: "",
      lname: "",
      email: "",
      password: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    console.log("Submit event");
    e.preventDefault();
    const { fname, lname, email, password } = this.state;
    console.log(fname, lname, email, password);
    fetch("http://localhost:1337/users/register", {
      method: "POST",
      crossDomain: true,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "Access-Control-Allow-Origin": "http://localhost:1337",
      },
      body: JSON.stringify({
        fname,
        email,
        lname,
        password,
      }),
      mode: 'no-cors'
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "userRegister");
      });
  }
  render() {
    return (
      // <form onSubmit={this.handleSubmit}> ตัดทิ้ง
        <div>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
              sx={{
                marginTop: 16,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography component="h1" variant="h5">
                Register
              </Typography>
              <Box
                component="form"
                noValidate
                onSubmit={this.handleSubmit}
                sx={{ mt: 3 }}
              >
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      name="fname"
                      required
                      fullWidth
                      id="fname"
                      label="First Name"
                      onChange={(e) => this.setState({ fname: e.target.value })}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      fullWidth
                      id="lname"
                      label="Last Name"
                      name="lname"
                      onChange={(e) => this.setState({ lname: e.target.value })}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      margin="normal"
                      required
                      fullWidth
                      id="email"
                      label="Enter email"
                      name="email"
                      // autoComplete="email"
                      // autoFocusvalue={inputs.email || ""}
                      onChange={(e) => this.setState({ email: e.target.value })}
                    />
                    <TextField
                      margin="normal"
                      required
                      fullWidth
                      name="password"
                      label="Password"
                      type="password"
                      id="password"
                      // autoComplete="current-password"
                      // value={inputs.password || ""}
                      onChange={(e) =>
                        this.setState({ password: e.target.value })
                      }
                    />

                      <input
                        // onClick={(e) => this.handleSubmit(e)}
                        type="submit"
                        variant="contained"
                        sx={{ mt: 1, mb: 1 }}
                      >
                      </input>
                    <Grid container></Grid>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Container>
        </div>
      // </form>
    );
  }
}
// export default Register;
