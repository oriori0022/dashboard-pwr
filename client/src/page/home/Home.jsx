import Chart from "../../component/Chart/Chart";
import Featured from "../../component/Featured/Featured";
import Navbar from "../../component/Navbar/Navbar";
import Sidebar from "../../component/Sidebar/Sidebar";
import Widget from "../../component/Widget/Widget";
import "./home.scss";


const Home = () => {
  return (
    <div className="home">
      <Sidebar />
      <div className="homeContainer">
        <Navbar />
       <div className="widget">
        <Widget type= "total view"/>
        <Widget type= "total user"/>
        <Widget type= "total sale"/>
       </div>
       <div className="charts">
        <Featured />
        <Chart /> 
       </div>
      </div>
    </div>
  );
};

export default Home;