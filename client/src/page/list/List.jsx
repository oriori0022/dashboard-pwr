import Datatable from "../../component/Datatable/Datatable"
import Navbar from "../../component/Navbar/Navbar"
import Sidebar from "../../component/Sidebar/Sidebar"
import "./list.scss"


const List = () => {
  return (
    <div className="list">
      <Sidebar />
      <div className="listContainer">
        <Navbar />
        <Datatable />
      </div>
    </div>
  )
}

export default List