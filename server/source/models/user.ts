import mongoose, { Schema } from 'mongoose';
import IUser from '../interfaces/user';

const UserSchema: Schema = new mongoose.Schema(
    {
        fname: { type: String, required: true},
        lname: { type: String, required: true},
        email: { type: String, required: true},
        password: { type: String, required: true }
    },
    {
        timestamps: true
    }
);
export default mongoose.model<IUser>('User', UserSchema);